package ie.ucd.lab4;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class Drinker extends Person {

	private int numberOfDrinks;
	private String name; //Leaves possibility to add name

	public Drinker(String name) {
		this.name = name;
	}

	@Override
	public boolean drink(Drink drinkType) {
		if (drinkType instanceof AlcoholicDrink) {
			this.numberOfDrinks++; // Increment the number of drinks if an alcoholic drink has been taken
			return true;
		} else {
			return true; // Need to return true to show a drink of any kind has been taken
		}
	}

	public boolean isDrunk() {
		if (numberOfDrinks > ((this.getWeight()) / 10)) { //Determine whether the person is drunk
			System.out.println("Go home you're drunk!");
			return true; //Returns true if the person is drunk
		} else {
			return false;
		}
	}

	@Override
	public boolean eat(Food arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
