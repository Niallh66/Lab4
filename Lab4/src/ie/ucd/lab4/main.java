package ie.ucd.lab4;

import ie.ucd.items.NotAlcoholicDrink;
import ie.ucd.items.Wine;

public class main {

	public main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Wine fancyFrench = new Wine(null, 0.1, 0.1, null);
		Drinker LocalDrunk = new Drinker("Tom");
		NotDrinker GoodCitizen = new NotDrinker("Sally");
		
		LocalDrunk.drink(fancyFrench);
		GoodCitizen.drink(fancyFrench);
		LocalDrunk.setWeight(2);
		LocalDrunk.isDrunk();
		LocalDrunk.drink(fancyFrench);
		LocalDrunk.drink(fancyFrench);
		LocalDrunk.drink(fancyFrench);
		LocalDrunk.isDrunk();
	
	}

}
