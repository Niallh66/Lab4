package ie.ucd.lab4;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class NotDrinker extends Person {
	
	private String name; //Leaves possibility to add name

	public NotDrinker(String name) {
		this.name = name;
	}

	@Override
	public boolean drink(Drink drinkType) {
		if (drinkType instanceof AlcoholicDrink) {
			System.out.println("Can't drink, alcohol is the Devil, you go to those meetings for a reason!");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean eat(Food arg0) {
		return false;
	}

}
